=========
Khám phá bảo tàng Oz
=========
.. meta::
   :description: Từ Ngày Tưởng niệm đến Ngày Lao động, bảo tàng mở cửa từ Thứ Hai đến Thứ Bảy, từ 9:00 sáng đến 6:00 chiều và Chủ nhật từ trưa đến 6:00 chiều Trong thời gian còn lại của năm.
   :keywords: bảo tàng Oz 

Một bộ sưu tập tuyệt vời của tất cả những thứ Oz nằm nép mình ở trung tâm của Kansas. Bạn không cần phải đợi một cơn lốc xoáy cuốn bạn đi đến vùng đất huyền diệu của xứ Oz, nhờ có Bảo tàng Oz, tọa lạc tại một thị trấn nhỏ của Kansas.

Được thành lập vào năm 2004 trên một khoản trợ cấp lớn từ bang Kansas, bảo tàng không chỉ kỷ niệm bộ phim nổi tiếng năm 1939, mà còn tôn vinh câu chuyện về xứ Oz như một hiện tượng văn hóa bắt đầu với cuốn sách thiếu nhi The Wonderful Wizard of Oz năm 1900 của L. Frank Baum.

*********
Oz mang đến sự sống
*********

Bộ sưu tập của bảo tàng chứa hàng nghìn đồ tạo tác và đồ sưu tầm liên quan đến Oz, chẳng hạn như những cuốn sách đầu tiên từ bộ truyện của Baum, trò chơi trên bàn cờ và trò chơi điện tử. Ngoài bộ sưu tập di vật và phù du ngày càng tăng, bảo tàng còn có một số trưng bày làm sống động các cảnh trong vở nhạc kịch Judy Garland.

.. image:: image/oz-museum.jpg
  :width: 650
  :alt: Ở tại một nhà nghỉ bằng thuyền / máy bay ở Alaska

Bạn có thể thấy một căn phòng được sơn giống như trang trại đen trắng của Dorothy, chuyển sang một căn phòng được chiếu sáng rực rỡ, nơi bạn có thể đi vài bước xuống con đường lát gạch màu vàng. Kể từ khi Bảo tàng Oz được thành lập, một cộng đồng nhỏ gồm các doanh nghiệp liên quan đến Oz đã xuất hiện ở khu vực xung quanh, chẳng hạn như một nhà máy rượu và một nhà hàng taco (Toto's Tacoz).

Thị trấn Wamego có thể nhỏ, nhưng với tất cả những điều kỳ diệu được gói gọn trong "cụm Oz", như cách gọi của người dân địa phương, du khách không thể đổ lỗi khi nghĩ rằng họ đã bước vào một thế giới ma thuật hoàn toàn khác.

*********
Biết trước khi bạn đi
*********

Từ Ngày Tưởng niệm đến Ngày Lao động, bảo tàng mở cửa từ Thứ Hai đến Thứ Bảy, từ 9:00 sáng đến 6:00 chiều và Chủ nhật từ trưa đến 6:00 chiều Trong thời gian còn lại của năm.

Bảo tàng mở cửa từ thứ Hai đến thứ Bảy từ 10:00 sáng. đến 5:00 chiều và Chủ nhật từ trưa đến 5:00 chiều Giá vé vào cửa cho người lớn là $ 9,00 và vé vào cửa cho trẻ em từ ba đến 12 tuổi là $ 7,00.