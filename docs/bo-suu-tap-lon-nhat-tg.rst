=========
Khám phá bộ sưu tập lớn nhất thế giới
=========
.. meta::
   :description: Vào năm 2017, Bộ sưu tập các phiên bản nhỏ nhất của những thứ lớn nhất lớn nhất thế giới đã mua lại một tòa nhà ở trung tâm thành phố ở Lucas, Kansas.
   :keywords: Alaska, du lịch Mỹ

Một điểm tham quan độc đáo và bảo tàng dành riêng cho việc khám phá các cấu trúc bên đường mang tính biểu tượng. Erika Nelson, người sáng tạo ra Bộ sưu tập lớn nhất thế giới của những thứ lớn nhất thế giới về phiên bản nhỏ nhất của thế giới

Đi du lịch nước Mỹ để tìm kiếm "những con quái vật trên đường" (những thứ như chai tương cà lớn nhất thế giới hoặc quả bóng bằng sợi). Sau đó, cô chụp ảnh chúng và tạo ra Phiên bản nhỏ nhất thế giới của Điều lớn nhất thế giới nói trên.

Sau khi Nelson nghiên cứu, tham quan và chụp ảnh Thứ lớn nhất thế giới, sau đó cô ấy tạo ra một bản sao thu nhỏ của "thứ". Khi có thể, Nelson sẽ đưa phiên bản nhỏ bé trở lại cấu trúc khổng lồ đã tạo cảm hứng cho nó và chụp một bức ảnh của hai món đồ với nhau. Các bản sao nhỏ và các bức ảnh sau đó được trưng bày trong bảo tàng và trực tuyến.

*********
Một nghệ sĩ tại nơi làm việc
*********

Nelson tạo ra những phiên bản nhỏ nhất thế giới của những thứ lớn nhất thế giới từ nhiều loại vật liệu. Ví dụ, khi tái tạo quả bóng bằng dây cao su lớn nhất thế giới, cô ấy đã sử dụng dây cao su thu nhỏ mà bạn tìm thấy ở văn phòng bác sĩ chỉnh nha.

Nelson là một nghệ sĩ, nhà giáo dục và là một trong những chuyên gia và diễn giả hàng đầu của Hoa Kỳ về Những Điều Lớn Nhất Thế Giới. Ngoài việc đến thăm các cộng đồng với bảo tàng du lịch độc đáo của riêng cô ấy, nơi đã có cơ sở lâu dài vào năm 2017.

.. image:: image/lucas-kansas-nelson.jpg
  :width: 650
  :alt: lucas kansasnelson

Nelson còn là nhà tư vấn cho các thành phố đang tìm cách tạo ra “Điều lớn nhất thế giới” hoặc điểm thu hút ven đường để tăng du lịch, tiếp thị và phát triển kinh tế cho cộng đồng. Khi không ở trên đường, Nelson và chiếc xe tải "Bộ sưu tập lớn nhất thế giới của những phiên bản nhỏ nhất thế giới của những thứ lớn nhất thế giới" có thể được tìm thấy ở Lucas, Kansas.

*********
Biết trước khi bạn đi
*********

Vào năm 2017, Bộ sưu tập các phiên bản nhỏ nhất của những thứ lớn nhất lớn nhất thế giới đã mua lại một tòa nhà ở trung tâm thành phố ở Lucas, Kansas.

Tòa nhà mới đã cho phép bộ sưu tập chuyển đổi từ màn hình du lịch và biểu ngữ trình chiếu sang một Hội chợ triển lãm bên đường mới. Du khách quan tâm đến bộ sưu tập có thể tìm thấy nó tại 214 South Main ở Lucas. Bảo tàng mở cửa tình cờ hoặc theo lịch hẹn.