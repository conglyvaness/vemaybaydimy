=========
3 khu dân cư LGBT phải xem ở Bờ Tây USA Hoa Kỳ
=========
.. meta::
   :description: Bắt đầu bằng chuyến viếng thăm Bảo tàng Lịch sử GLBT, bảo tàng thường trực đầu tiên dành riêng cho văn hóa và lịch sử LGBT ở Hoa Kỳ Đi theo Phố Castro để tìm Đại lộ Danh vọng LGBT, hay còn gọi là Rainbow Honor Walk, với các tấm bia vỉa hè bằng đồng của các cá nhân LGBT nổi bật.
   :keywords: vé máy bay đi mỹ

Nổi lên như là trung tâm của phong trào dân quyền đồng tính nữ, đồng tính nam, song tính và chuyển giới (LGBT) trong những năm 1960 và 1970, trải nghiệm Bờ biển phía Tây Hoa Kỳ là nơi có nhiều điểm đến mang tính biểu tượng và tiến bộ.

Đặt mua vé máy bay đi Mỹ khám phá xa lộ Liên tiểu bang 5 thuận tiện liên kết các thành phố lớn giữa California và Washington, cung cấp một tuyến đường dễ dàng đến thăm các khu phố LGBT lịch sử và các điểm tham quan kỷ niệm để tôn vinh các quyền dân sự ở Hoa Kỳ
   
*********
West Hollywood — Los Angeles, California
*********   

Để tìm trung tâm của văn hóa LGBT của miền Nam California, hãy đến West Hollywood ở Los Angeles. Tại đây, bạn sẽ tìm thấy các bảo tàng LGBT chuyên dụng như Kho lưu trữ đồng tính nữ tháng sáu L. Mazer, với các tài liệu về đồng tính nữ, tác phẩm nghệ thuật, video và quần áo.

.. image:: image/west-hollywood.jpg
  :width: 650
  :alt: West Hollywood — Los Angeles, California

Bên cạnh, bạn sẽ tìm thấy MỘT Phòng trưng bày và Bảo tàng Lưu trữ, bảo tàng đầu tiên ở Nam California dành riêng cho lịch sử đồng tính, nơi lưu giữ một bộ sưu tập đồ sộ về các tài liệu lịch sử và văn hóa LGBT. Vào buổi tối, đi dạo trên đại lộ Santa Monica, một quán bar và câu lạc bộ trải dài như cầu vồng bao gồm Tu viện, một quán bar đồng tính lâu đời từ năm 1971 nổi tiếng với martini, khiêu vũ, nhìn thấy người nổi tiếng và nhạc sống.

Vào tháng 6, West Hollywood tổ chức LA ba ngày ! Pride Music Festival & Parade, lễ hội tự hào LGBT đầu tiên trên thế giới và lớn nhất ở Nam California, có các buổi biểu diễn âm nhạc, các bữa tiệc, triển lãm và, tất nhiên, một cuộc diễu hành có màu cầu vồng.

*********
The Castro - San Francisco, California
*********   

Là ngôi nhà của một trong những quần thể LGBT lớn nhất ở Hoa Kỳ, Quận Castro là một khu phố lịch sử không thể bỏ qua ở San Francisco, California. Nổi lên vào những năm 1960 như một thành trì của hoạt động, cộng đồng mang tính bước ngoặt này đã tạo ra một nơi tôn nghiêm cho người LGBT định cư, tiến hành kinh doanh và giao tiếp.

Bắt đầu bằng chuyến viếng thăm Bảo tàng Lịch sử GLBT, bảo tàng thường trực đầu tiên dành riêng cho văn hóa và lịch sử LGBT ở Hoa Kỳ Đi theo Phố Castro để tìm Đại lộ Danh vọng LGBT, hay còn gọi là Rainbow Honor Walk, với các tấm bia vỉa hè bằng đồng của các cá nhân LGBT nổi bật.

Cũng trên con phố này là Harvey Milk Plaza, được đặt tên cho người đồng tính nam công khai đầu tiên được bầu vào văn phòng công cộng ở California, và được đánh dấu bằng lá cờ cầu vồng dài 21 mét, một bức tranh tường về Sữa và một tấm bia tưởng niệm cửa hàng Camera trước đây của ông. Sau đó, dừng chân tại Twin Peaks Tavern, quán bar đồng tính đầu tiên ở San Francisco chính thức được chỉ định với trạng thái mốc.

*********
Tam giác Burnside và Hawthorne - Portland, Oregon
*********

.. image:: image/hawthorne-portland.jpg
  :width: 650
  :alt: Tam giác Burnside và Hawthorne - Portland, Oregon

Khi ở Portland, Oregon, chuyến thăm tới Burnside Triangle và Hawthorne, hai khu phố LGBT của thành phố, nên có trong danh sách việc cần làm của bạn. Trong những năm 1960 và 70, các câu lạc bộ đồng tính nam và đồng tính nữ đã xuất hiện trong các khu vực này và tiếp tục hoạt động ngày nay với vô số cửa hàng, nhà hàng, dịch vụ xã hội, nhà hát và cuộc sống về đêm của người đồng tính.

Ăn một miếng tại The Roxy Diner, một nhà hàng bình thường được biết đến với dịch vụ thân thiện với người đồng tính và các món ăn trong thực đơn kỳ quặc như bánh sandwich GLBT, một riff trên BLT (thịt xông khói, rau diếp, bánh mì cà chua) được làm bằng thịt xông khói đồng tính. Tháng 5 là một tháng tốt để đến thăm và đón xem Red Dress Party, một buổi dạ tiệc gây quỹ nổi tiếng về AIDS trong đó hơn 2.000 người tham dự mặc một chiếc váy đỏ, bất kể giới tính.