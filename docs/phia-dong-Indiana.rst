=========
Du lịch khám phá đường thủy phía đông Indiana Mỹ
=========
.. meta::
   :description: Cuộc đua có chiều dài khoảng 2.000 feet và chỉ mất khoảng năm phút để đi bè. Tất cả mọi người từ những người chèo thuyền kayak có kinh nghiệm đến khách du lịch vấp ngã có thể tận dụng những vùng nước gồ ghề..
   :keywords: Indiana, du lịch Mỹ

Sau nhiều năm không sử dụng, kênh đã được lấp đầy, và kể từ những năm 1970, dấu vết duy nhất của cửa sông St. Joseph trước đây là lan can cầu vô hại vẫn còn tồn tại giữa khu vực trung tâm thành phố thất bại.

Book vé máy bay giá rẻ đi Mỹ khám phá đường thủy được xây dựng vào những năm 1840 để cung cấp năng lượng cho xưởng cưa cho ngành công nghiệp sản xuất thịnh vượng của South Bend. Bây giờ nó là một dòng sông nước trắng thương mại và cạnh tranh chạy thẳng qua một trung tâm thành phố nhộn nhịp.

Bất chấp sự phản đối mạnh mẽ của một số người nộp thuế, East Race Waterway thời Nội chiến đã được hồi sinh sau khi được lấp đầy và san bằng, một hành động ban đầu dường như báo hiệu sự kết thúc của ngày. Khóa học nước trắng nhân tạo đầu tiên ở Bắc Mỹ cắt ngang qua một thành phố ở Indiana.   

*********
Biết trước khi bạn đi
*********

Đường thủy mở cửa cho công chúng và là một phần của con đường đi bộ từ Đại học Indiana South Bend. Nó cũng thân thiện với chó và thân thiện với xe đạp.

Xem trang web của đường thủy để biết thêm chi tiết về cách đặt chuyến đi và lời khuyên cho những gì mong đợi.

Tất cả các thiết bị đi bè được cung cấp, mặc dù những người chèo thuyền kayak phải có thiết bị riêng của họ. Một lần đi xe duy nhất có giá 6 đô la.

.. image:: image/south-bend-indiana-my.jpg
  :width: 650
  :alt: Du lịch khám phá Frankenmuth Michigan

*********
Một đường thủy được khôi phục
*********

Cuộc đua có chiều dài khoảng 2.000 feet và chỉ mất khoảng năm phút để đi bè. Tất cả mọi người từ những người chèo thuyền kayak có kinh nghiệm đến khách du lịch vấp ngã có thể tận dụng những vùng nước gồ ghề.

Nó hiện đang mở cửa cho công chúng, và bất cứ ai cũng có thể thuê một chiếc tàu và cố gắng tự lái tàu. Vào thời điểm đó, nó là khóa học nước trắng nhân tạo duy nhất ở Bắc Mỹ. Ngày nay, đường thủy đã hoàn thành mục đích của nó và khu vực này một lần nữa là một không gian trung tâm thành phố sôi động với kênh nằm ở trung tâm.

Đến năm 1984, với sự hỗ trợ của tiền thuế của người dân, tuyến đường thủy Undead đã được sửa đổi và đưa vào sử dụng bởi những người chèo thuyền nước trắng cạnh tranh. Sau đó, vào năm 1978, với sự hỗ trợ của thị trưởng địa phương, một kế hoạch đã được đưa ra để nâng kênh đào từ cõi chết như một phần của kế hoạch phục hồi lớn cho khu vực.