=========
2 thú vui tuyệt đỉnh không thể bỏ lỡ ở Alaska
=========
.. meta::
   :description: Gói lưu trú qua đêm có tính năng đi tàu trong ngày bắt đầu ngày khởi hành có sẵn. Đi thuyền kayak và câu cá có sẵn với chi phí bổ sung cho người qua đêm; chúng là một phần của thỏa thuận nếu bạn ở lại nhiều hơn một đêm.
   :keywords: Alaska, du lịch Mỹ
   

*********
Ở tại một nhà nghỉ bằng thuyền / máy bay
*********

Giá bắt đầu $ 1,300 mỗi đêm và bao gồm ba bữa ăn do đầu bếp chuẩn bị mỗi ngày, mát-xa một giờ, nếm rượu vang, yoga và hầu hết mọi hoạt động được hướng dẫn bạn có thể nghĩ đến - chèo thuyền kayak, đi bộ đường dài đến sông băng, đi xe đạp leo núi, câu cá địa phương và thuyền các chuyến đi, đi bộ tự nhiên, và các lớp học nấu ăn.

Book vé máy bay đi New York khám phá đảo Fox là điểm dừng chân trên hai chuyến du thuyền trong ngày của Kenai Fjords, nhưng bạn có thể ở lại qua đêm tại Kenai Fjords Wilderness Lodge. Khách sạn bao gồm tám cabin (mỗi cabin có sức chứa cho một gia đình bốn người) xếp hàng giữa bãi đá và đầm nước phía sau.

.. image:: image/Alaska-hoa-ky.jpg
  :width: 650
  :alt: Ở tại một nhà nghỉ bằng thuyền / máy bay ở Alaska

Tutka là một trong những vịnh hẹp được cắt vào phía nam của Vịnh Kachemak lớn hơn, và toàn bộ khu vực này có những đỉnh núi tuyết và những rặng núi phủ kín Sitka chạy thẳng ra đại dương. Nhà nghỉ được đặt trở lại trên một bãi biển đối diện với một vùng đất nhỏ - bạn không thể nhìn thấy nó cho đến khi bạn gần như ở trên nó.

Gói lưu trú qua đêm có tính năng đi tàu trong ngày bắt đầu ngày khởi hành có sẵn. Đi thuyền kayak và câu cá có sẵn với chi phí bổ sung cho người qua đêm; chúng là một phần của thỏa thuận nếu bạn ở lại nhiều hơn một đêm.

Nhưng một khi bạn ở đó, nó khá rộng, với một tầng trung tâm đồ sộ (có bồn tắm nước nóng và phòng tắm hơi), và các lối đi kết nối tòa nhà trọ chính và sáu cabin sang trọng với kích cỡ khác nhau. The Eagle's Nest Chalet (ngủ năm) có lẽ có tầm nhìn tốt nhất.

Ở phía đối diện bán đảo Kenai, Tutka Bay Lodge thậm chí còn có cảm giác xa hơn với nó, được truy cập bằng taxi nước từ Homer Spit hoặc máy bay biển.

*********
Bay lên núi leo lên nếu bạn có thể
*********

Đây cũng là cách những người leo núi tiếp cận những ngọn núi. Để biết thông tin về leo núi, hãy kiểm tra trang tài nguyên leo núi của Công viên Quốc gia.

Phạm vi Alaska xác định địa hình của tiểu bang, một cột sống hình lưỡi liềm uốn cong từ biên giới phía đông nam với Canada, ở phía nam Fairbanks và quay trở lại biển ở cửa Cook Inlet.

.. image:: image/fox-alaska-usa.jpg
  :width: 650
  :alt: Bay lên núi leo lên nếu bạn có thể ở Fox Alaska Mỹ

Để làm điều đó, bạn cần một chiếc máy bay. Một số ít các công ty điều hành các chuyến tham quan trên chuyến bay trực tuyến ra khỏi Talkeetna, K2 Hàng không là lớn nhất. Cũng có thể bay từ khu vực Denali. Bất cứ ai bạn bay với, đăng ký một hạ cánh sông băng cho hiệu ứng đầy đủ.

Thu hút tầm nhìn là tốt đẹp; một trong những nơi tốt nhất để làm như vậy là từ khu vực boong sau của Talkeetna Alaskan Lodge. Nhưng bạn có một viễn cảnh hoàn toàn khác khi bạn thực sự ở trên núi, đứng trên sông băng, nhìn lên và nhìn xung quanh vào một thế giới trắng lởm chởm.

Tuy nhiên, phần lớn mọi người biết và ghé thăm là khu vực xung quanh Denali, đỉnh cao nhất của Bắc Mỹ với 6,193,5 mét, và hai nước láng giềng của nó, Foraker (5.303,5 mét) và Hunter (4.256,5 mét).